import random
import pandas as pd

path = 'words.xlsx'
file_obj = ""
posicion = 0
word_description = ""

def getWords(file = 'words.xlsx'):
    try:
        global file_obj
        file_obj = pd.read_excel(file)
        dataList =  file_obj.values.tolist()
        return dataList
    except FileNotFoundError:
        print(f"File '{file}' not founf.")
    except Exception as e:
        print(f"Error: {e}")

def sumOneToCol(posicion, column, quantity = 1):
    file_obj.at[posicion, column] += quantity
    file_obj.to_excel(path, index=False)

def printDescriptionWord(word):
    print("\n*****************\n")
    print("Meaning\n")
    print(word+": "+word_description+"\n")
    print("*****************")

def wordToPlay():
    global posicion
    global word_description
    words = getWords()
    posicion = random.randint(1, len(words)-1)
    word_description = words[posicion][1]
    return list(words[posicion][0].upper()) 

def hangman():
    """
    The function `hangman()` allows the user to guess letters in a word and keeps track of their
    attempts and successes.
    """
    attempts_counter = 0  
    sumOneToCol(posicion, "plays")
    
    while True:
        try:
            letter = str(input("\nType one letter: "))
            letter = letter.upper()

            if (attempts-1) == attempts_counter:
                sumOneToCol(posicion, "failed")
                print("\n ¡You lose.")
                break
            
            actual_word = " ".join(user_words)

            if letter in app_word:
                for i in range(len(app_word)):
                    if app_word[i] == letter:
                        user_words[i] = letter

                actual_word = " ".join(user_words)
                print("\n "+actual_word)
            else:
                attempts_counter+=1
                print("\n "+actual_word)
                print("\n You have "+str(attempts - attempts_counter)+" attempts left")
            
            if app_word == user_words:
                sumOneToCol(posicion, "success")
                print("\n Winner.")
                break

        except Exception as e:
            print(f"Error: {e}")


app_word = wordToPlay()
word_len = len(app_word)

user_words = ["_"] * word_len

attempts = set(app_word)
attempts = len(attempts) + 1

print("Welcome to Hangman! \n")

print("You guess to word of  "+
    str(word_len)+" letter. You have "+
    str(attempts)+" attempts"+"\n "
)

hangman()
printDescriptionWord(" ".join(app_word))

