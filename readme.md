# Hangman

The code is a simple word guessing game where the player has to guess a word by inputting letters, and the code keeps track of the number of attempts and updates the game statistics in an Excel file.

** This is a costeñol word version ** 

## Install

pip install -r requirements.txt

You can edit the word file and add new words or change all existing ones.

## Challenge 

Develop a new strategy to choose a word from the words file that makes each attempt more challenging than the previous one, while still incorporating randomness.